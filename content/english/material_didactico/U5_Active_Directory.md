---
title: "Introducción a Active Directory"
date: 2022-10-23T00:00:00+00:00
author: PHCT
image_webp: images/material/blog/u5_intro_ad.png
image: images/blog/material/u5_intro_ad.png
description : "Unidad 5. Introducción a Active Directory."
categories: ["Material didactico"]
tags: ["Active Directory", "Windows", "Kerberos", "Golden Ticket"]
---

## Introducción a Active Directory

En la actualidad, Active Directory es un sistema ampliamente utilizado en la industria. Conocer cómo funciona y sus vulnerabilidades es de gran utilidad en el pentesting. 

### Material didáctico

[Active Directory](https://gitlab.com/PumaHat/pumahat.gitlab.io/-/tree/main/content/english/material/U5_Intro_AD)
