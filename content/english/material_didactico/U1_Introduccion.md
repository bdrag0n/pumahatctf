---
title: "Introducción a la ciberseguridad"
date: 2022-10-23T00:00:00+00:00
author: PHCT
image_webp: images/blog/material/u1_intro.webp
image: images/blog/material/u1_intro.webp
description : "Unidad 1. Introducción a la ciberseguridad."
categories: ["Material didactico"]
tags: ["pentesting", "cyberkill chain", "white hat", "linux", "redes"]
---

## Introducción

La ciberseguridad es un campo muy amplio. Es necesario tener una noción básica de otros temas: el sistema operativo GNU/Linux, redes de datos, aplicaciones y peticiones web, etc. 

### Material didáctico

[Introduccion](https://gitlab.com/PumaHat/pumahat.gitlab.io/-/tree/main/content/english/material/U1_Introduccion)