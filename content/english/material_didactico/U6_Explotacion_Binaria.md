---
title: "Explotación binaria."
date: 2022-10-23T00:00:00+00:00
author: PHCT
image_webp: images/blog/material/u6_explotacion_binaria.jpg
image: images/blog/material/u6_explotacion_binaria.jpg
description : "Unidad 6. Explotación binaria."
categories: ["Material didactico"]
tags: ["ensamblador", "binario", "reversing", "buffer overflow"]
---

## Explotación binaria

Fuera de las vulnerabilidades que ocurren debido a un error de configuración, existen las vulnerabilidades debido a un error en el programa mismo. Para saber identificar estas vulnerabilidades, es necesario entender el código a bajo nivel, los binarios del ejecutable. Existen varias vulnerabilidades comunes que nos permiten explotar el código, tales como los buffer overflows.

### Material didáctico

[Explotación Binaria](https://gitlab.com/PumaHat/pumahat.gitlab.io/-/tree/main/content/english/material/U6_Explotacion_Binaria)