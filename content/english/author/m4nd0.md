---
title: "m4nd0"
image: "images/author/masked_man.jpeg"
social:
  - icon : "ti-twitter-alt" # themify icon pack : https://themify.me/themify-icons
    link : "https://twitter.com/0xAVG"
  - icon : "ti-github" # themify icon pack : https://themify.me/themify-icons
    link : "https://github.com/0xm4nd0/"
---

¡Hola, qué tal! Yo soy m4nd0, un ***newbie*** que tiene muchas ganas de aprender y adentrarse en el campo de la ciberseguridad. 
Actualmente me enfoco a estudiar temas de ***Pentesting*** y seguridad ofensiva, pero entre mis intereses igual se encuentran ***Reverse Engineering*** y el análisis de malware.