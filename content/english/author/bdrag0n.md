---
title: "BDrag0n"
image: "images/author/bdrag0n.jpg"
social:
  - icon : "ti-twitter-alt" # themify icon pack : https://themify.me/themify-icons
    link : "https://twitter.com/BDrag0n_x"
---

Soy BDrag0n, me apasiona la ciberseguridad, software libre y TI en general. Entre mis intereses principales está el Pentesting y DFIR.
Don't learn to hack, hack to learn.
